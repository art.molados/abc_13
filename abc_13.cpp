﻿/* 1. Cоздайте двумерный массив размерности NxN и заполните его так, 
чтобы элемент с индексами i и j был равен i + j. */
// 2. Выведите этот массив в консоль. 
/* 3. Выведите сумму элементов в строке массива, 
индекс которой равен остатку деления текущего числа календаря на N 
(в двумерном массиве a[i][j], i — индекс строки). */

#include <iostream>
#include <ctime>
#pragma warning(disable : 4996) 
using namespace std;


int day()
{
    time_t t = time(0);   // get time now
    tm* now = localtime(&t);
    int day = now->tm_mday;
    return day;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    const int A = 3;                    //Количество строк
    const int B = 3;                    //Количество столбцов
    int element[A][B];                   //Массив 5х5

    //Создание матрицы
    for (int i = 0; i < A; ++i)
        for (int j = 0; j < B; ++j)
            element[i][j] = i + j;

    //Вывод чисел
    for (int i = 0; i < A; ++i)          //прибавка +1, с 1 до I
    {                                    
        for (int j = 0; j < B; ++j)      //прибавка +1, с 1 до J
            cout << element[i][j];       //вывод массива с табуляцией
        cout << "\n";                    //следующая строка
    }
    
    int row = day() % A;
    int rows = sizeof element / sizeof element[0];                  //Длина массива, деленная на длину первого измерения
    if (row >= rows)
    {
        cout << "Нужная строка " << row << " но в массиве всего " 
            << rows << " строк. Введите другое значение N";
        return 0;
    }
    int elementsInRow = sizeof element[0] / sizeof element[0][0];   //количество столбцов двумерного массива. 
    int sum = 0;                                                    //Объявляем переменную, в которую запишем сумму.
        for (int j = 0; j < elementsInRow; j++)
             {
                  sum += element[row][j];                           //row - наша строка, которую мы получили, найдя остаток от деления ДЕНЬ на N
             }
        cout << sum << endl;    
}
